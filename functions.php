<?php

    function load_stylesheets(){

        wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), false, 'all');
        wp_enqueue_style('bootstrap');

        wp_register_style('style', get_template_directory_uri() . '/style.css', array(), false, 'all');
        wp_enqueue_style('style');

        
    }
    add_action('wp_enqueue_scripts', 'load_stylesheets');




    function include_jquery()
{

    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', get_template_directory_uri() . '/js/jquery-3.4.1.min.js', 1, true);

    add_action('wp_enqueue_scripts', 'jquery');

}
add_action('wp_enqueue_scripts', 'include_jquery');

    function loadjs()
    {
        wp_register_script('customjs', get_template_directory_uri() . '/js/scripts.js', '', 1, true);
        wp_enqueue_script('customjs');

    }
    add_action('wp_enqueue_scripts','loadjs');


add_theme_support('menus');
add_theme_support('post-thumbnails');


register_nav_menus(

    array(

        'top-menu' => __('Top Menu', 'theme'),
        'footer-menu' => __('Footer Menu','theme'),

    )

);

function _namespace_menu_item_class($classes, $item) {       
    $classes[] = "nav-item"; 
    $classes[] = "nav-link"; 
    return $classes;
} 


add_filter( 'nav_menu_css_class' , '_namespace_menu_item_class' , 10, 2 );


add_image_size('smallest', 300, 300, true);
add_image_size('largest', 500, 500, true);


function my_widget(){
    register_sidebar( array(
        'name' => __( 'Front Sidebar', 'theme' ),
        'id' => 'sidebar-1',
        'description' => __( 'This is description', 'theme' ),
        'before_widget' => '<aside>',
        'after_widget' => '</aside>',
        'before_title' => '<h6><b>',
        'after_title' => '</h6></b>',
    ) );
}
add_action( 'widgets_init', 'my_widget' );


function base_pagination() {
    global $wp_query;

    $big = 999999999;
    $paginate_links = paginate_links( array(
        'base' => str_replace( $big, '%#%', get_pagenum_link($big) ),
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages,
        'mid_size' => 5
    ) );

    if ( $paginate_links ) {
            echo '<div class="pagination">';
            echo $paginate_links;
            echo '</div><!--// end .pagination -->';
    }
}
